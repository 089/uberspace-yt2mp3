<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get the submitted URL from the form
    $url = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_URL);

    if ($url) {
        // Check if the URL contains youtube.com or youtu.be
        if (strpos($url, 'youtube.com') === false && strpos($url, 'youtu.be') === false) {
            // If not, add "# " at the beginning of the line
            $url = '# ' . $url;
        }

        // Open the text file (create if not exists) in append mode
        $file = fopen('urls.txt', 'a');

        if ($file) {
            // Write the URL to the file
            fwrite($file, $url . PHP_EOL);

            // Close the file
            fclose($file);

            echo 'URL has been successfully added. <a href="index.html">redo</a>';
        } else {
            echo 'Unable to open the file. <a href="index.html">retry</a>';
        }
    } else {
        echo 'Invalid URL.';
    }
} else {
    // Redirect to the form if accessed directly
    header('Location: index.html');
    exit();
}
?>
