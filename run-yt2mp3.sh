#!/bin/bash

cd ~/bin/uberspace-yt2mp3

# update repository
## Fetch the latest changes from the remote repository
git fetch origin

## Reset the local branch to match the remote branch
git reset --hard origin/main

# Set the source and destination directories
SOURCE_DIR="yt2mp3"
DEST_DIR="/var/www/virtual/$USER/html"
FILENAME="urls.txt"

# Check if the destination directory exists, create it if not
if [ ! -d "$DEST_DIR" ]; then
    mkdir -p "$DEST_DIR"
    echo "Destination directory created: $DEST_DIR"
fi

# Copy the source directory to the destination
cp -r "$SOURCE_DIR" "$DEST_DIR"

# Display a message indicating success
echo "Directory '$SOURCE_DIR' copied to '$DEST_DIR' successfully."

# install or update yt-dlp to home directory (cf.: https://manual.uberspace.de/lang-python/?highlight=pip )
pip3.9 install --upgrade --quiet yt-dlp --user

######################
# diff lists of urls #
######################

# create files if not exits
touch "${FILENAME}.curr"
touch "${FILENAME}.new"
touch "${DEST_DIR}/${SOURCE_DIR}/${FILENAME}"

# copy url list to local directory
cp -f "${DEST_DIR}/${SOURCE_DIR}/${FILENAME}" "${FILENAME}.new"

# diff files
comm -3 <(sort "${FILENAME}.new") <(sort "${FILENAME}.curr") > "${FILENAME}.diff"
echo "process following URLs ..."
cat "${FILENAME}.diff"

# new file is new curr file
cp -f "${FILENAME}.new" "${FILENAME}.curr"

# download youtube videos
DOWNLOADS_DIR="downloads"
mkdir -p "${DOWNLOADS_DIR}"
$HOME/.local/bin/yt-dlp -o "${DOWNLOADS_DIR}/%(title)s_%(id)s.%(ext)s" --restrict-filenames --extract-audio --audio-format mp3 --batch-file "${FILENAME}.diff"


# upload to public nextcloud share
touch config.sh
source config.sh

# Loop durch MP3-Dateien im Download-Verzeichnis
for mp3_file in "${DOWNLOADS_DIR}"/*.mp3; do
    if [ -f "${mp3_file}" ]; then
        # Extrahiere den Dateinamen
        filename=$(basename "${mp3_file}")

        # Führe den Curl-Befehl aus, um die Datei hochzuladen
        curl -k -T "${mp3_file}" -u "${NEXTCLOUD_PUBLIC_USER}:" -H 'X-Requested-With: XMLHttpRequest' "${NEXTCLOUD_URL}${filename}"
        curl_exit_code=$?

        # Überprüfe den Erfolg des Curl-Befehls
        if [ $curl_exit_code -eq 0 ]; then
            echo "Upload erfolgreich: $filename"

            # Lösche die MP3-Datei nach erfolgreichem Upload
            rm "$mp3_file"
            echo "Datei gelöscht: ${filename}"
        else
            echo "Fehler beim Upload von $filename. Curl exit code: $curl_exit_code"
        fi
    fi
done
